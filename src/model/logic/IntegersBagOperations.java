package model.logic;

import java.util.Iterator;

import model.data_structures.IntegersBag;

public class IntegersBagOperations{
	
	////////////////////////////////////////////
	//M�TODOS ORIGINALES
	////////////////////////////////////////////
	public double computeMean(IntegersBag bag){
		double mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				mean += iter.next();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}


	public int getMax(IntegersBag bag){
		int max = Integer.MIN_VALUE;
		int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( max < value){
					max = value;
				}
			}

		}
		return max;
	}

	
	////////////////////////////////////////////
	//M�TODOS NUEVOS
	////////////////////////////////////////////
	
	
	//OBTIENE EL MINIMO
	public int getMin(IntegersBag bag){
		int min = Integer.MAX_VALUE;
		int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( min > value){
					min = value;
				}
			}

		}
		return min;
	}
	
	//OBTIENE EL MAXIMO PAR
	public int getMaxPar(IntegersBag bag){
		int max = Integer.MIN_VALUE;
		int value;
		int guardian = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( max < value && value%2==0)
				{
					max = value;
					guardian = 1;
				}
			}

		}
		if(guardian==0)
		{
			return guardian;
		}
		return max;
	}
	
	//OBTIENE EL MINIMO IMPAR
	public int getMinImpar(IntegersBag bag){
		int min = Integer.MAX_VALUE;
		int value;
		int guardian = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( min > value && min%2==1)
				{
					min = value;
					guardian=1;
				}
			}
		}
		if(guardian==0)
		{
			return guardian;
		}
		return min;
	}
	
	
}
